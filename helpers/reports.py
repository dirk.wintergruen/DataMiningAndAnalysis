from collections import defaultdict


def getSourceInfo(solr,pk):
    source = {}
    solr_info = {}

    res = solr.search(q="id:documents.source.%s " %pk ,fl="id,text")
    for r in res:
        solr_info["id"] = r["id"]
        if "text" in r:
            solr_info["text_indexed"] = True
            solr_info["text_len"] = len(r["text"])
        else:
            solr_info["text_indexed"] = False

    res = solr.search(q="sources:%s AND django_ct:documents.page" % pk, fl="id,type,text" ,rows=3000)
    pages_index = defaultdict(int)
    for r in res:
        t = r.get("type" ,None)
        if t:
            pages_index[t] += 1
    source["source"] = solr_info
    source["pages_indexed"] = pages_index
    return source
