import argparse
import json
import logging
from multiprocessing.pool import Pool

import requests
import spacy as spacy
from textblob_de import TextBlobDE as TextBlob
from textblob_de import Word
from tqdm import tqdm

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

import pysolr

LIMIT = 1000
#SOLR_URL = "http://localhost:28983/solr/gmpg-test"

nlp = spacy.load("de")

def spacy(txt):
    ents = {}
    ents["log_spacy_de_str"] = []
    ents["per_spacy_de_str"] = []
    ents["misc_spacy_de_str"] = []
    ents["loc_spacy_de_str"] = []
    ents["org_spacy_de_str"] = []

    try:
        spacy = nlp(txt)
    except ValueError:
        return ents

    for ent in spacy.ents:
        ents["%s_spacy_de_str" % ent.label_.lower()].append(ent.text.strip())
    return ents

def getTxt(txt):
    txt = TextBlob(txt)
    return txt


def prepare_words_singular_de(txt):
    ret = []
    if txt is None:
        return ret
    for l, c in txt.word_counts.items():
        s = str(Word(l).singularize()).strip()
        #singularized[l] = s
        ret.append(s)
    return ret

def prepare_sentences_de(txt):
    ret = []
    if txt is None:
        return ret
    for l in txt.sentences:
        ret.append(str(l).strip())
    return ret

def prepare_tags_de(txt):
    ret = []
    for t in txt.tags:
        ret.append(json.dumps(t))
    return ret

def prepare_nounphrases_de(txt):
    ret = []
    for sent in txt.sentences:
        for np in sent.noun_phrases:  # nounphrase in text is different from noun_phrase of each sentence
            s = str(np.singularize())
            # singularized[np] = s
            for cnt in range(0, txt.noun_phrases.count(np)):
                ret.append(s)  # append as often as it occurs
    return ret

def addMultSpacy(solr,qs,fields_in,workers=2):

    solr_server = pysolr.Solr(solr)
    fl = ",".join(fields_in)
    res = solr_server.search(qs, fl="id," + fl, rows=1)
    print("I found %s hits." % res.hits)

    batch = int(res.hits / workers)
    batches = []
    for start in tqdm(range(0, res.hits, batch)):
        batches.append([solr,start,start+batch,qs,fields_in])
    with Pool(len(batches)) as p:
        p.map(addSpacyPool,batches)


def addSpacyPool(args):

    solr_url, start, end, qs, fields_in = args
    solr = pysolr.Solr(solr_url)
    print("START: %s" % start)

    fl = ",".join(fields_in)

    rs = solr.search(qs, fl="id," + fl, rows=end-start, start=start)
    cnt = 0
    update = []
    for r in tqdm(rs):
        cnt += 1
        up = {"id": r["id"]}
        title_values = []
        txts = []
        for f in fields_in:
            if not f in r:
                continue
            if isinstance(r[f], list):
                txts.append(" ".join(r[f]))
            else:
                txts.append(r[f])

        txt = " ".join(txts)
        ents = spacy(txt)
        for k, v in ents.items():
            up[k] = {"set": v}
        update.append(up)
        if cnt > 10:
            res = requests.post(solr_url + "/update", json=update)
            cnt = 0
            update = []
            print(res.content)

    solr.commit()


def addMultTextBlob(solr,qs,fields_in,workers=2):

    solr_server = pysolr.Solr(solr)
    fl = ",".join(fields_in)
    res = solr_server.search(qs, fl="id," + fl, rows=1)
    print("I found %s hits." % res.hits)

    batch = int(res.hits / workers)
    batches = []
    for start in tqdm(range(0, res.hits, batch)):
        batches.append([solr,start,start+batch,qs,fields_in])
    with Pool(len(batches)) as p:
        p.map(addTextBlob,batches)



def addTextBlob(args):
    solr_url, start, end, qs, fields_in = args
    solr = pysolr.Solr(solr_url)
    print("START: %s" % start)

    fl = ",".join(fields_in)

    rs = solr.search(qs, fl="id," + fl, rows=end - start, start=start)
    cnt = 0
    update = []
    for r in tqdm(rs):
        cnt += 1
        up = {"id": r["id"]}
        title_values = []
        txts = []
        for f in fields_in:
            if not f in r:
                continue
            if isinstance(r[f], list):
                txts.append(" ".join(r[f]))
            else:
                txts.append(r[f])

        txt = " ".join(txts)

        txt = getTxt(txt)
        up["words_singular_de"] = {"set": prepare_words_singular_de(txt)}
        up["sentences_de"] = {"set": prepare_sentences_de(txt)}
        up["tags_de"] = {"set": prepare_tags_de(txt)}
        up["nounphrases_de"] = {"set": prepare_nounphrases_de(txt)}

        update.append(up)
        if cnt > 10:
            res = requests.post(solr_url + "/update", json=update)
            cnt = 0
            update = []
            print(res.content)

    solr.commit()


    solr = pysolr.Solr(solr_url)

    fl = ",".join(fields_in)
    res = solr.search(qs,fl="id,"+fl,rows=1)
    print("I found %s hits."%res.hits)

    for start in range(0,res.hits,LIMIT):
        update = []
        rs = solr.search(qs,fl="id,"+fl,rows=LIMIT,start=start)
        for r in rs:
            up = {"id": r["id"]}
            title_values = []
            txt =" ".join([r[f] for f in fields_in])
            txt = getTxt(txt)
            up["words_singular_de"] = {"set":prepare_words_singular_de(txt)}
            up["sentences_de"] = {"set": prepare_sentences_de(txt)}
            up["tags_de"] = {"set": prepare_tags_de(txt)}
            up["nounphrases_de"] = {"set": prepare_nounphrases_de(txt)}

            update.append(up)
        res = requests.post(solr_url + "/update", json=update)
        print(res.content)
    solr.commit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--solr', help='solr url', required=True)
    parser.add_argument('--workers', help='number of parallel tasks', default=2, type=int)
    parser.add_argument('--qs', help="query string default: django_ct:documents.source", default="django_ct:documents.source")
    parser.add_argument('--type', help="spacy or textblob - default spacy",
                        default="spacy")
    #parser.add_argument('--overwrite', help="overwrite existing entries -- not implemented yet",
    #                    default=False,const=True,nargs="?")

    args = parser.parse_args()

    if args.type == "spacy":
        addMultSpacy(solr=args.solr,qs=args.qs,fields_in=["text"],workers=args.workers)
    elif args.type == "textblob":
        addMultTextBlob(solr=args.solr, qs=args.qs, fields_in=["text"], workers=args.workers)
    else:
        print("don not know type: %s"%args.type)


