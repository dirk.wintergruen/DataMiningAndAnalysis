"""
Convert txt fields (or others) to field of different kind.
"""
import argparse
import json
from collections import defaultdict

import logging

import requests
from tqdm import tqdm

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

import pysolr

LIMIT = 50
#SOLR_URL = "http://localhost:28983/solr/gmpg-test"

def guessMetaData(solr_url,qs):
    solr = pysolr.Solr(solr_url)

    fields_in=["md_akten_bestand_archiv_bezeichnung_txts",
               "md_akten_bestand_signatur_txts",
               "md_akten_aktensignatur_box_txts",
               "md_akten_bestand_archiv_bezeichnung_txts_s",
               "md_akten_bestand_signatur_txts_s",
               "md_akten_aktensignatur_box_txts_s",
               "url"]

    fl = ",".join(fields_in)
    res = solr.search(qs,fl="id,"+fl,rows=1)
    print("I found %s hits."%res.hits)

    for start in range(0,res.hits,LIMIT):
        update = []
        rs = solr.search(qs,fl="id,"+fl,rows=LIMIT,start=start)
        for r in tqdm(rs):

            up = {"id":r["id"]}

            url = r.get("url",None)
            if not url:
                continue

            if url.startswith("/"):
                url = url[1:]

            url_splitted = url.split("/")

            do_update = False
            if len(r.get("md_akten_bestand_archiv_bezeichnung_txts",[])) == 0:
                up["md_akten_bestand_archiv_bezeichnung_txts"] = {"set" : [url_splitted[0]]}
                up["md_akten_bestand_archiv_bezeichnung_txts_s"] = {"set" :url_splitted[0]}
                do_update = True

            if len(url_splitted)>1:



                if len(r.get("md_akten_bestand_signatur_txts",[])) == 0:
                    up["md_akten_bestand_signatur_txts"] = {"set" :[url_splitted[1]]}
                    up["md_akten_bestand_signatur_txts_s"] ={"set" : url_splitted[1]}
                    do_update = True

                if len(r.get("md_akten_aktensignatur_box_txts",[])) == 0:
                    up["md_akten_aktensignatur_box_txts"] = {"set" :["/".join(url_splitted[2:])]}
                    up["md_akten_aktensignatur_box_txts_s"] = {"set" :"/".join(url_splitted[2:])}
                    do_update = True

                if len(r.get("md_akten_bestand_signatur_txts_s", [])) == 0:
                    up["md_akten_bestand_signatur_txts"] = {"set": [url_splitted[1]]}
                    up["md_akten_bestand_signatur_txts_s"] = {"set": url_splitted[1]}
                    do_update = True

                if len(r.get("md_akten_aktensignatur_box_txts_s", [])) == 0:
                    up["md_akten_aktensignatur_box_txts"] = {"set": ["/".join(url_splitted[2:])]}
                    up["md_akten_aktensignatur_box_txts_s"] = {"set": "/".join(url_splitted[2:])}
                    do_update = True
            else:
                pass

            if do_update:
                update.append(up)
        if len(update) > 0:
            res = requests.post(solr_url+"/update", json=update)
            print(res.content)
    solr.commit()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--solr', help='solr url', required=True)

    args = parser.parse_args()

    guessMetaData(solr_url=args.solr,qs="django_ct:documents.source AND -md_akten_bestand_archiv_bezeichnung_txts:*")








    
    