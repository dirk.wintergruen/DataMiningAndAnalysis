"""
Convert txt fields (or others) to field of different kind.
"""
import argparse
import json
from collections import defaultdict
import re
import logging

import requests
from tqdm import tqdm

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

import pysolr

LIMIT = 50
#SOLR_URL = "http://localhost:28983/solr/gmpg-test"

def guessMetaData(solr_url,qs,pattern=r"/([1-2][03-9][0-9][0-9])/"):
    solr = pysolr.Solr(solr_url)

    fields_in=["url","md_akten_laufzeit_start_txts","md_akten_laufzeit_end_txts"]
    fields_find = ["url"]
    re_pattern = re.compile(pattern)
    fl = ",".join(fields_in)
    res = solr.search(qs,fl="id,"+fl,rows=1)
    print("I found %s hits."%res.hits)


    for start in range(0,res.hits,LIMIT):
        update = []
        rs = solr.search(qs,fl="id,"+fl,rows=LIMIT,start=start)
        for r in tqdm(rs):

            up = {"id":r["id"]}
            do_update = False
            for f in fields_find:
                vgl_string = r[f]
                find = re_pattern.findall(vgl_string)
                dates = []
                for fnd in find:
                    dates.append(int(fnd))


                if len(dates) > 0:
                    d_max = max(dates)
                    d_min = min(dates)

                    if len(r.get("md_akten_laufzeit_start_txts",[])) == 0:
                        up["md_akten_laufzeit_start_txts"] = {"set": d_min}
                        do_update = True
                    if len(r.get("md_akten_laufzeit_end_txts",[])) == 0:
                        up["md_akten_laufzeit_end_txts"] = {"set": d_max}
                        do_update = True

            if do_update:
                update.append(up)
        if len(update) > 0:
            res = requests.post(solr_url+"/update", json=update)
            print(res.content)
    solr.commit()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--solr', help='solr url', required=True)

    args = parser.parse_args()

    guessMetaData(solr_url=args.solr,qs="django_ct:documents.source AND url:*dfg*")








    
    