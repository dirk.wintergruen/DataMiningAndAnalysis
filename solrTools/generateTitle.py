"""
Convert txt fields (or others) to field of different kind.
"""
import argparse
import json
from collections import defaultdict

import logging

import dateparser
import requests

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

import pysolr

LIMIT = 1000
#SOLR_URL = "http://localhost:28983/solr/gmpg-test"

def generateTitle(solr,qs,fields_in,field_out):
    solr = pysolr.Solr(solr)

    fl = ",".join(fields_in)
    res = solr.search(qs,fl="id,"+fl,rows=1)
    print("I found %s hits."%res.hits)

    for start in range(0,res.hits,LIMIT):
        update = []
        rs = solr.search(qs,fl="id,"+fl,rows=LIMIT,start=start)
        for r in rs:

            up = {"id":r["id"]}
            title_values = []
            for f in fields_in:
                if r.get(f,None):
                    if isinstance(r[f],list):
                        value = "-".join(r[f])
                    else:
                        value = r[f]
                    title_values.append(value)

            up[field_out] = {"set":"-".join(title_values)}

            update.append(up)

        res = requests.post(solr+"/update", json=update)
        print(res.content)
    solr.commit()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--solr', help='solr url', required=True)

    args = parser.parse_args()

    # for x in candidates:
    #     convert2intOrFloatOrDate(qs="%s:*"%x,fl="%s"%x)
    fields_in=["md_akten_bestand_archiv_signatur_txts",
               "md_akten_bestand_signatur_txts",
               "md_akten_bestand_abteilung_signatur_txts",
               "md_akten_aktensignatur_box_txts",
               "md_file_class_value_txts",
               "md_file_altsignatur_txts",
               "md_akten_bestand_archiv_signatur_txt",
               "md_akten_bestand_signatur_txt",
               "md_akten_bestand_abteilung_signatur_txt",
               "md_akten_aktensignatur_box_txt",
               "md_file_class_value_txt",
               "md_file_altsignatur_txt"

               ]
    generateTitle(solr=args.solr,qs="django_ct:documents.source",fields_in=fields_in,field_out="title")








    
    