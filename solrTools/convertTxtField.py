"""
Convert txt fields (or others) to field of different kind.
"""
import argparse
import json
from collections import defaultdict

import logging

import dateparser
import requests
from tqdm import tqdm

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

import pysolr

LIMIT = 1000


def convert2intOrFloatOrDate(solr_url,qs,fl,date=True,date_out=None):
    solr = pysolr.Solr(solr_url)
    res = solr.search(qs,fl="id,"+fl,rows=1)
    print("I found %s hits."%res.hits)

    for start in range(0,res.hits,LIMIT):
        update = []
        rs = solr.search(qs,fl="id,"+fl,rows=LIMIT,start=start)
        for r in tqdm(rs):

            try:
                xs = r[fl]
            except KeyError:
                print(r)
                continue
            multivalued = True
            if not isinstance(xs,list):
                multivalued = False
            elif len(xs) == 1:
                multivalued = False
                xs = xs[0]
            elif len(xs) == 0:
                continue
            if multivalued:
                suf = "s"
                update_data = defaultdict(list)
                for t in xs:
                    try:
                        x = int(t)
                        if not x in update_data["i"]:
                            update_data["i"].append(x)
                    except:
                        try:
                            x = float(t)
                            if not x in update_data["f"]:
                                update_data["f"].append(x)
                        except:
                            pass
                    if date == True:
                        dt = dateparser.parse(t)
                        if str(dt).split(" ")[0].strip() == "":
                            continue

                        update_data["dt"].append(str(dt).split(" ")[0])
            else:
                update_data = defaultdict(str)
                suf = ""
                try:
                    x = int(xs)
                    update_data["i"] = x
                except:
                    try:
                        x = float(xs)
                        update_data["f"] = x
                    except:
                        pass

                if date == True:
                    dt = dateparser.parse(xs)
                    if str(dt).split(" ")[0].strip() == "":
                        continue
                    update_data["dt"] = str(dt).split(" ")[0]
            up = {"id":r["id"]}

            if update_data["i"]:
                up[fl+"_l"+suf] = {"set":update_data["i"]}
            if update_data["f"]:
                  up[fl+"_d"+suf] = {"set":update_data["f"]}


            if date:
                up[fl+"_dr"+suf] = {"set":update_data["dt"]} #take only the date
            if date_out:
                if isinstance(update_data["dt"],list):
                    update_data["dt"] = update_data["dt"][0] #I want to use this as date for sorting
                up[date_out] = {"set": update_data["dt"]+"T00:00:00Z"}  # take only the date

            update.append(up)

        res_update = requests.post(solr_url+"/update", json=update)
        print(res_update.content)
    solr.commit()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--solr', help='solr url', required=True)

    candidates=["md_akten_date_initial_txts",
                "md_akten_laufzeit_start_txts",
                "md_akten_laufzeit_end_txts",
               "md_file_laufzeit_von_txt",
               "md_file_laufzeit_bis_txt"]

    args = parser.parse_args()
    for x in candidates:
        convert2intOrFloatOrDate(solr_url=args.solr,qs="url:*dfg* AND %s:*"%x,fl="%s"%x)
        #convert2intOrFloatOrDate(qs="id:documents.source.232568", fl="%s" % x, date_out="date")

    x ="md_akten_laufzeit_start_txts"
    convert2intOrFloatOrDate(solr_url=args.solr,qs="url:*dfg* AND %s:*" % x, fl="%s" % x,date_out="date")








    
    