from setuptools import setup

setup(
    name='DataMiningAndAnalysis',
    version='',
    packages=['helpers', 'solrTools'],
    url='',
    license='',
    author='dwinter',
    author_email='',
    description='Tools for enriching the solr index'
)
